const add = (a, b) => a + b;

describe('calculator', function() {
    describe('add', function(){
        it('should exist', () => {
            add();
        })
        it('should add 2 + 2 = 4', () => {
            const result = add(2, 2);
            expect(result).toBe(4);
        })
        it('should add 10 + 10 = 20', function() {
            const result = add(10, 10);
            expect(result).toBe(20);
        });
    });
});