import {fizzBuzz} from "./FizzBuzz";

const checkFizzBuzzResult = (input, expected) => {
    const result = fizzBuzz(input);
    expect(result).toBe(expected)
};

describe('FizzBuzz function', () => {
    it('exists', () => {
        fizzBuzz();
    });

    it('returns 1 for 1' ,() => {
        checkFizzBuzzResult(1, 1);
    });

    it('returns "Fizz" for 3', () => {
        checkFizzBuzzResult(3, 'Fizz');
    });

    it('returns "Buzz" for 5', () => {
        checkFizzBuzzResult(5, "Buzz");
    });

    it('returns "FizzBuzz" for 15', () => {
        checkFizzBuzzResult(15, "FizzBuzz");
    });
});