export const fizzBuzz = number => {
    // let result = '';
    // if(number % 3 === 0){
    //     result += 'Fizz';
    // }
    // if(number % 5 === 0){
    //     result += "Buzz";
    // }
    // return result === '' ?  number : result;

    if(number % 15 === 0){
        return "FizzBuzz";
    }
    if(number % 5 === 0){
        return "Buzz";
    }
    if(number % 3 === 0){
        return "Fizz";
    }

    return number;
};