import usersSlice from "../slices/usersSlice";
import {historyPush} from "./historyActions";
import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";

export const {
  registerUser,
  registerUserSuccess,
  registerUserFailure,
  loginUser,
  loginUserSuccess,
  loginUserFailure,
  googleLoginRequest,
  logoutUser,
  logoutRequest,
  clearErrorUser,
} = usersSlice.actions;

export const facebookLogin = data => {
  return async dispatch => {
    try {
      const response = await axiosApi.post('/users/facebookLogin', data);
      dispatch(loginUserSuccess(response.data.user));
      dispatch(historyPush('/'));
      toast.success('Login successful');
    } catch (error) {
      toast.error(error.response.data.global);
      dispatch(loginUserFailure(error.response.data));
    }
  };
};
