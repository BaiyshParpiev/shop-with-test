import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  user: null,
  registerLoading: false,
  registerError: null,
  loginError: null,
  loginLoading: false,
};

const name = 'users';

const usersSlice = createSlice({
  name,
  initialState,
  reducers: {
    registerUser: (state, action) => {
      state.registerLoading = true;
    },
    registerUserSuccess: (state, {payload: userData}) => {
      state.user = userData;
      state.registerLoading = false;
      state.registerError = null;
    },
    registerUserFailure: (state, {payload: user}) => {
      state.registerLoading = false;
      state.registerError = user;
    },
    loginUser: (state) => {
      state.loginLoading = true;
    },
    loginUserSuccess: (state, {payload: user}) => {
      state.loginError = null;
      state.loginLoading = false;
      state.user = user;
    },
    loginUserFailure: (state, {payload: error}) => {
      state.loginError = error;
      state.loginLoading = false
    },
    googleLoginRequest: (state) => {
      state.loginLoading = true;
    },
    clearErrorUser: (state) => {
      state.registerError = null;
      state.loginError = null;
    },
    logoutRequest: () => {},
    logoutUser: (state) => {
      state.user = null;
    },
  }
});

export default usersSlice;