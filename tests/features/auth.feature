Feature: Sign up user
  As anonymous user
  I have to be able to sign up to the system
  When I enter my data

  @register
  Scenario: Registration
    Given I go to the "register" page
    And I write my data:
      | email       | test@test.com |
      | password    | 123qwerty     |
      | displayName | Test Testov   |
    When click on to the button "Sign up"
    Then I see text "Registered successful!"

    @login
  Scenario: Login
    Given I go to the "login" page
    And I write my data:
      | email       | admin@gmail.com |
      | password    | 1qaz@WSX29    |
    When click on to the button "Sign in"
    Then I see text "Login successful"