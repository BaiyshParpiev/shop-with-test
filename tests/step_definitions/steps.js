const {I} = inject();

Given('I go to the {string} page', (page) => {
    I.wait(30)
    I.amOnPage('/' + page);
});

Given('I write my data:', (table) => {
    table.rows.forEach(row => {
        const name = row.cells[0].value
        const value = row.cells[1].value

        I.fillField(name, value);
    });
});

When('click on to the button {string}', (buttonText) => {
    if (buttonText === "Sign up"  || buttonText === "Sign in" ) {
        I.click(`//form//button[.//span[contains(text(), '${buttonText}')]]`);
    }else{
        I.click(buttonText);
    }
});

Then('I see text {string}', (textAboutSuccess) => {
    I.see(textAboutSuccess);
});


Then('I have a defined step', () => {

});

